<?php

namespace Isha\CentralBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IshaCentralBundle extends Bundle
{
    /**
     * Current Bundle version.
     *
     * @var string
     */
    const VERSION = '0.1.0-BETA';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
    }
}