<?php

namespace Isha\CentralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Controllers for Anonymous Index Page
 */
class IndexController extends Controller
{
    public function indexAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ( $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') or $securityContext->isGranted('IS_AUTHENTICATED_FULLY') ) {
            return $this->redirect($this->generateUrl('home'));
        } else {
            $this->get("isha_breadcrumbs.factory")->reset('Index');
            return $this->render('IshaCentralBundle:Index:home.html.twig');
        }
    }
}