<?php

namespace Isha\CentralBundle\Controller;

use Isha\CentralBundle\Form\Type\SadhanaType;
use Isha\YogaCenterBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Controllers for Anonymous Index Page
 */
class PersonController extends Controller
{
    /**
     * View the payrolls for this month.
     *
     * @Security( "has_role('ROLE_SADHANA') " )
     * @return Response
     */
    public function sadhanaByDepartmentAction()
    {
        $this->get("isha_breadcrumbs.factory")->update('View Sadhana Breaks ');

        $suvyaEm = $this->getDoctrine()->getManager('suvya');

        $user = $this->getUser();
        $meAsPerson = $user->getPerson();
        $myDepartments = $suvyaEm->getRepository('IshaYogaCenterBundle:IshaDepartment')->findByMyDepartments($meAsPerson);

        /// TODO: get team members directly from database
        // in array format
        $people = array();
        foreach ($myDepartments as $department) {
            $teams = $department->getIshaTeams();
            foreach ($teams as $team) {
                $members = $team->getTeamMembers();
                foreach ($members as $member) {
                    if ($member->getCategory() != Person::STAFF) {
                        $people[] = $member;
                    }
                }
            }
        }

        return $this->render('IshaCentralBundle:Person:sadhana.html.twig', array(
            'people'=> $people
        ));
    }

    /**
     * @Security( "has_role('ROLE_SADHANA')")
     */
    public function addSadhanaAction(Request $request,$person_id,$type)
    {
        $suvyaEm = $this->getDoctrine()->getManager('suvya');
        $person = $suvyaEm->getRepository('IshaYogaCenterBundle:Person')->findOneById($person_id);

        switch ($type) {
            case 'silence':
                $person->type = 'Silence';
                break;
            case 'program_volunteering':
                $person->type = 'Program Volunteering';
                break;
            case 'seva':
                $person->type = 'Seva';
                break;
        }

        $form = $this->createForm(SadhanaType::class, $person);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $suvyaEm->persist($person);
            $suvyaEm->flush();
            return new JsonResponse(array('message' => 'Success!','entityId'=>$person->getId()), 200);
        }

        $new_url = $this->generateUrl('add_sadhana_to_person',array('person_id'=>$person_id,'type'=>$type));

        $response = new JsonResponse(
            array(
                'message' => 'Hello',
                'form' => $this->renderView('IshaUIBundle::ajax_form.html.twig',
                    array(
                        'entity' => $person,
                        'form' => $form->createView(),
                        'page_title'  => 'Adding Sadhana',
                        'action_path' => $new_url,
                        'submit_button_title'  => 'Add Sadhana'
                    ))), 200);
        return $response;
    }

    /**
     * @Security( "has_role('ROLE_SADHANA')")
     */
    public function mostRecentSadhanaAction($person_id,$type)
    {
        $suvyaEm = $this->getDoctrine()->getManager('suvya');
        $person = $suvyaEm->getRepository('IshaYogaCenterBundle:Person')->findOneById($person_id);

        $mostRecentSadhana = $person->getMostRecentSadhana($type);
        $duration = 'N / A';
        $due = 'N / A';
        $from = 'N / A';
        if ($mostRecentSadhana != null){
            $duration = $mostRecentSadhana['duration'];
            $due = $mostRecentSadhana['due'];
            $from = $mostRecentSadhana['from'];
            $from = $from->format('d M Y');
        }

        $response = new JsonResponse(
            array('duration'=>$duration,'due'=>$due,'from'=>$from), 200);
        return $response;
    }
}