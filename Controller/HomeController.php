<?php

namespace Isha\CentralBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Isha\HRBundle\Entity\ReviewForm;
use Isha\YogaCenterBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Administration.
 */
class HomeController extends Controller
{
    /**
     * Home page for either department lead or hr admin
     *
     * @param Request $request
     * @param bool $mis
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function homeAction(Request $request,$ishaDepartment_id=null)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        } else {

            $this->get("isha_breadcrumbs.factory")->reset('Home');

            $hrEm = $this->getDoctrine()->getManager('hr');
            $suvyaEm = $this->getDoctrine()->getManager('suvya');

            $user = $this->getUser();
            $meAsPerson = $user->getPerson();
            $myDepartments = $suvyaEm->getRepository('IshaYogaCenterBundle:IshaDepartment')->findByMyDepartments($meAsPerson);
            $allDepartments = $suvyaEm->getRepository('IshaYogaCenterBundle:IshaDepartment')->findAll();

            $year = 2016;

            $ratings = $hrEm->getRepository('IshaHRBundle:Rating')->findByYear($year);

            $personCounter = 0;
            $volunteerCounter = 0;

            $currentCTC = array();
            $finalCTC = array();
            $deptCount = 0;

            foreach ($myDepartments as $department) {
                $deptCount += 1;

                $subDepartments = $department->getIshaDepartments();
                if (count($subDepartments) == 0) {
                    $teams = $department->getIshaTeams();
                    foreach ($teams as $team) {
                        $members = $team->getTeamMembers();
                        foreach ($members as $person) {
                            if ($person->getDateOfExit() != null) {
                                continue;
                            }
                            $personCounter += 1;
                            if ($person->getCategory() != Person::STAFF) {
                                $volunteerCounter += 1;
                            } else {
                                $financialRecord = $hrEm->getRepository('IshaHRBundle:FinancialRecord')->findOneBy(array('personId' => $person->getId(), 'year' => $year));
                                if ($financialRecord != null) {
                                    $currentCTC[$person->getId()] = $financialRecord->getCurrentCTC();
                                    $finalCTC[$person->getId()] = $financialRecord->getFinalCTC($ratings);
                                }
                            }
                        }
                    }
                } else {
                    foreach ($subDepartments as $_department) {
                        $teams = $_department->getIshaTeams();
                        foreach ($teams as $team) {
                            $members = $team->getTeamMembers();
                            foreach ($members as $person) {
                                if ($person->getDateOfExit() != null) {
                                    continue;
                                }
                                $personCounter += 1;
                                if ($person->getCategory() != Person::STAFF) {
                                    $volunteerCounter += 1;
                                } else {
                                    $financialRecord = $hrEm->getRepository('IshaHRBundle:FinancialRecord')->findOneBy(array('personId' => $person->getId(), 'year' => $year));
                                    if ($financialRecord != null) {
                                        $currentCTC[$person->getId()] = $financialRecord->getCurrentCTC();
                                        $finalCTC[$person->getId()] = $financialRecord->getFinalCTC($ratings);
                                    }
                                }
                            }
                        }
                        $deptCount += 1;
                    }
                }
            }

            if ($meAsPerson != null) {
                if ($ishaDepartment_id == null) {
                    $leadingDepartment = $suvyaEm->getRepository('IshaYogaCenterBundle:IshaDepartment')->findByLeadingDepartment($meAsPerson);
                    if (count($leadingDepartment) > 0) {
                        $leadingDepartment = $leadingDepartment[0];
                    } else {
                        $leadingDepartment = null;
                    }
                } else {
                    $leadingDepartment = $suvyaEm->getRepository('IshaYogaCenterBundle:IshaDepartment')->findOneById($ishaDepartment_id);

                    $collection = new ArrayCollection($myDepartments);

                    if ($collection->contains($leadingDepartment) == false) {
                        throw new NotFoundHttpException;
                    }

                }

                $peopleGraph = null;
                if ($leadingDepartment != null) {
                    $peopleGraph = $suvyaEm->getRepository('IshaAnalyticsBundle:Graph')->findOneByHandleBar('department_card_growth_' . $leadingDepartment->getId());
                    if ($peopleGraph != null) {
                        $peopleGraph = json_encode($peopleGraph->getData());
                    }
                }

            } else {
                $leadingDepartment = null;
                $peopleGraph = null;
            }

            $currentCTC = array_sum(array_values($currentCTC));
            $finalCTC = array_sum(array_values($finalCTC));

            return $this->render('IshaCentralBundle:Home:home.html.twig', array(
                'personCounter'=> $personCounter,
                'myDepartments'=> $deptCount,
                'departments'=> $myDepartments,
                'manpowerCosts'=> 0,
                'currentCTC'=>$currentCTC,
                'finalCTC'=> $finalCTC,
                'volunteerCounter'=> $volunteerCounter,
                'allDepartments'=> $allDepartments,
                'leadingDepartment'=> $leadingDepartment,
                'peopleGraph'=> $peopleGraph,
                'mis_active'=> ($ishaDepartment_id != null)
            ));

        }
    }

}