<?php

namespace Isha\CentralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Isha\AdminBundle\Entity\User as IshaBaseUser;

/**
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="Isha\CentralBundle\Repository\UserRepository")
 * @ORM\Table(name="isha_admin_user")
 *
 */
class User extends IshaBaseUser
{
    /**
     * All payrolls for this month
     *
     * @ORM\Column(name="payroll_departments", type="simple_array", nullable=true)
     */
    protected $payrollDepartments;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getPayrollDepartments()
    {
        return $this->payrollDepartments;
    }

    /**
     * @param mixed $payrollDepartments
     */
    public function setPayrollDepartments($payrollDepartments)
    {
        $this->payrollDepartments = $payrollDepartments;
    }
}