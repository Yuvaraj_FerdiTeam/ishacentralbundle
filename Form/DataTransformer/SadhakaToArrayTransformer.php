<?php

/*
 * This file is part of the IshaSuvyaBundle package.
 *
 * (c) Isha <info@Isha.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Isha\CentralBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Custom Document metadata transformer
 */
class SadhakaToArrayTransformer implements DataTransformerInterface
{

    protected $person;

    /**
     * {@inheritdoc}
     */
    public function transform($person)
    {
        $this->person = $person;

        if ($person === null) {
            return null;
        }

        return array(
            'type'=> $person->type
        );
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($array)
    {
        if ($array === null) {
            return null;
        }

        $person = $this->person;

        $from = $array['from'];
        $to = $array['to'];
        $type = $person->type;
        $comments = $array['comments'];

        $sadhana = array(
            'from'=>$from,
            'to'=> $to,
            'type'=>$type,
            'comments'=>$comments,
        );

        $currentSilence = $person->getSadhana();
        $currentSilence[] = $sadhana;
        $person->setSadhana($currentSilence);

        return $person;
    }
}
