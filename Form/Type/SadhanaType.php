<?php

namespace Isha\CentralBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Isha\CentralBundle\Form\DataTransformer\SadhakaToArrayTransformer;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SadhanaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('from',DateType::class,array(
                'widget'=>'single_text',
                'format'=>'dd-MM-yyyy',
                'required' => true,
                'attr'=>array('class'=>'inline-datepicker is-date','data-startview'=>'decade','readonly'=>true,'data-property'=>'to')
            ))
            ->add('to',DateType::class,array(
                'widget'=>'single_text',
                'format'=>'dd-MM-yyyy',
                'required' => true,
                'attr'=>array('class'=>'inline-datepicker is-date','data-startview'=>'decade','readonly'=>true)
            ))
            ->add('comments', TextareaType::class,array(
                'required'=>false,
                'label'=> 'Comments',
                'attr'=>array('data-property'=>'comments')
            ));

        $builder->addModelTransformer(new SadhakaToArrayTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    public function getName()
    {
        return 'sadhana';
    }
}