<?php

namespace Isha\CentralBundle\DataFixtures\Faker\Provider;

class RoleProvider
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }
    
    public static function superAdminRoles()
    {
        $basic_roles = array('ROLE_SUPER_ADMIN','ROLE_ADMIN');
        return $basic_roles;
    }

}