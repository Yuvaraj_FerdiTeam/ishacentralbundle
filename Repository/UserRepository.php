<?php

/*
 * This file is part of the IshaSuvyaBundle package.
 *
 * (c) Isha <info@Isha.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Isha\CentralBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ORM Repository for Entity\User.
 */
class UserRepository extends EntityRepository
{
    public function findByNonAdmin()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('u')
            ->from('IshaCentralBundle:User', 'u')
            ->andWhere('u.roles NOT LIKE :roles')
            ->setParameter('roles', '%' . 'ADMIN' . '%');

        $result = $qb->getQuery()->execute();
        return $result;
    }

    public function findByAdmin()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('u')
            ->from('IshaCentralBundle:User', 'u')
            ->andWhere('u.roles LIKE :roles')
            ->andWhere('u.roles NOT LIKE :super_roles')
            ->setParameter('roles', '%ADMIN%')
            ->setParameter('super_roles', '%SUPER_ADMIN%');

        $result = $qb->getQuery()->execute();
        return $result;
    }

    public function findByAttendanceManager()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('u')
            ->from('IshaCentralBundle:User', 'u')
            ->andWhere('u.roles LIKE :roles')
            ->andWhere('u.roles NOT LIKE :super_roles')
            ->setParameter('roles', '%STAFF_ATTENDANCE%')
            ->setParameter('super_roles', '%STAFF_ATTENDANCE_ADMIN%');

//        die();
        $result = $qb->getQuery()->execute();
        return $result;
    }

    public function findByWagesApproval()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('u')
            ->from('IshaCentralBundle:User', 'u')
            ->andWhere('u.roles LIKE :roles')
            ->setParameter('roles', '%TIME_CARDS%');

        $result = $qb->getQuery()->execute();
        return $result;
    }
}